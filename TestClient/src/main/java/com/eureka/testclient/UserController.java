package com.eureka.testclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/test/users")
	public List<User> getUsers() {
		String url = "http://EUREKACLIENT/users";

		ResponseEntity<List<User>> response = restTemplate.exchange(//
				url, //
				HttpMethod.GET, //
				null, //
				new ParameterizedTypeReference<List<User>>() {
				});

		return response.getBody();
	}
}
