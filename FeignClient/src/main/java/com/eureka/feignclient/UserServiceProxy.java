package com.eureka.feignclient;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "EUREKACLIENT")
public interface UserServiceProxy {

	@GetMapping("users")
	public List<User> getUsers();

	@GetMapping("user/{id}")
	public User getUser(@PathVariable("id") int id);
}
