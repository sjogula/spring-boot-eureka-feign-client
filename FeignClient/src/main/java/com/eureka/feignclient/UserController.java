package com.eureka.feignclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableFeignClients
public class UserController {
	@Autowired
	private UserServiceProxy serviceProxy;

	@GetMapping("/feign/users")
	public List<User> getAllUsers() {
		return serviceProxy.getUsers();
	}
	@GetMapping("/feign/user/{id}")
	public User getUser(@PathVariable("id") int id) {
		return serviceProxy.getUser(id);
	}
}
