package com.eureka.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	private List<User> users = Arrays.asList(new User(1, "abc", 10000), new User(2, "lmn", 20000),
			new User(3, "xyz", 30000));

	@GetMapping("/users")
	public List<User> getUsers() {
		return users;
	}

	@GetMapping("/user/{id}")
	public User getUser(@PathVariable("id") int id) {
		User user = new User();
		for (User userObj : users) {
			if (userObj.getId() == id) {
				user = userObj;
			}
		}
		return user;
	}
}
